Agorazein is TurboGears based website for conferences, it has been
created to provide a ready made framework for people that want to
set up a conference website in a bunch of hours.

Agorazein is developed and used by `AXANT <http://www.axant.it>`_
for some open source events organized in northern Italy.

Installation and Setup
======================

Agorazein requires `TurboGears 2.3 <http://www.turbogears.org>`_
make sure you installed the framework before installing agorazein itself.

Install ``agorazein`` using the setup.py script::

    $ cd agorazein
    $ python setup.py develop

Create the project database for any model classes defined::

    $ gearbox setup-app

Start the paste http server::

    $ gearbox serve

While developing you may want the server to reload after changes in package files (or its dependencies) are saved. This can be achieved easily by adding the --reload option::

    $ gearbox serve --reload --debug

Then you are ready to go.


Configuring
=======================

Agorazein provides a bunch of options to configure the conference
skeleton, have a look at ``development.ini`` ``conference.*`` options
and ``talk_options.json`` to tune the environment.
