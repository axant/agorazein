# -*- coding: utf-8 -*-

# turbogears imports
from tg import expose, redirect, validate, flash, config, lurl, require, request, abort
from tg.i18n import ugettext as _
from tg import predicates

import tw2.forms as twf
import tw2.core as twc

@expose('agorazein.templates.attendees.talks')
def talks(user):
    talks = user.talks
    if not request.identity or request.identity['user'] != user:
        talks = talks.filter_by(accepted=True)
    return dict(talks=talks.all())