# -*- coding: utf-8 -*-
"""Setup the agorazein application"""
from __future__ import print_function

import logging
from tg import config
from agorazein import model
import transaction

from smallpress.model import Blog, Article
from calendarevents.model import Calendar

def bootstrap(command, conf, vars):
    """Place any commands to setup agorazein here"""

    # <websetup.bootstrap.before.auth
    from sqlalchemy.exc import IntegrityError
    try:
        u = model.User()
        u.user_name = 'manager'
        u.display_name = 'Example manager'
        u.email_address = 'manager@somedomain.com'
        u.password = 'managepass'
        model.DBSession.add(u)
    
        g = model.Group()
        g.group_name = 'managers'
        g.display_name = 'Managers Group'
        g.users.append(u)
        model.DBSession.add(g)

        model.DBSession.flush()

        model.DBSession.add(Calendar(name='Talks', events_type='talk'))
        model.DBSession.add(Calendar(name='Deadlines', events_type='deadline'))
        model.DBSession.flush()

        model.DBSession.add(Blog(name='news'))
        statics_blog = Blog(name='statics')
        model.DBSession.add(statics_blog)

        model.DBSession.add(Article(blog=statics_blog, title='Where', published=True, author=u,
                                    content='where the conference is located'))
        model.DBSession.add(Article(blog=statics_blog, title='Who is coming', published=True, author=u,
                                    content=''''who is coming to the conference and special guests
browse full list of <a href="/attendees">speakers</a>'''))
        model.DBSession.add(Article(blog=statics_blog, title='Sponsors', published=True, author=u,
                                    content='List your conference sponsors here'))
        model.DBSession.flush()

        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your auth data, it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    # <websetup.bootstrap.after.auth>
