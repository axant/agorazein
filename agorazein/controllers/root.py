# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl, request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg import predicates
from agorazein import model
from agorazein.controllers.talks import TalksController
from agorazein.controllers.attendees import AttendeesController
from agorazein.model import DBSession, metadata
from tgext.admin.tgadminconfig import TGAdminConfig
from tgext.admin.controller import AdminController

from agorazein.lib.base import BaseController
from agorazein.controllers.error import ErrorController

from agorazein.model.posts import get_recent_news
from agorazein.model.calendar import get_upcoming_events, get_deadlines_calendar

__all__ = ['RootController']


class RootController(BaseController):
    admin = AdminController(model, DBSession, config_type=TGAdminConfig)
    error = ErrorController()

    attendees = AttendeesController()
    talks = TalksController()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "agorazein"

    @expose('agorazein.templates.index')
    def index(self, **kw):
        deadlines_calendar = get_deadlines_calendar()
        upcoming_deadlines = get_upcoming_events(deadlines_calendar)

        recent_news = get_recent_news()

        return dict(deadlines_calendar=deadlines_calendar, upcoming_deadlines=upcoming_deadlines,
                    recent_news=recent_news)

    @expose('agorazein.templates.about')
    def about(self, **kw):
        """Handle the 'about' page."""
        return dict(page='about')

    @expose('agorazein.templates.environ')
    def environ(self):
        """This method showcases TG's access to the wsgi environment."""
        return dict(page='environ', environment=request.environ)

    @expose('agorazein.templates.data')
    @expose('json')
    def data(self, **kw):
        """This method showcases how you can use the same controller for a data page and a display page"""
        return dict(page='data', params=kw)
    @expose('agorazein.templates.index')
    @require(predicates.has_permission('manage', msg=l_('Only for managers')))
    def manage_permission_only(self, **kw):
        """Illustrate how a page for managers only works."""
        return dict(page='managers stuff')

    @expose('agorazein.templates.index')
    @require(predicates.is_user('editor', msg=l_('Only for the editor')))
    def editor_user_only(self, **kw):
        """Illustrate how a page exclusive for the editor works."""
        return dict(page='editor stuff')

    @expose('agorazein.templates.login')
    def login(self, came_from=lurl('/')):
        """Start the user login."""
        login_counter = request.environ.get('repoze.who.logins', 0)
        if login_counter > 0:
            flash(_('Wrong credentials'), 'warning')
        return dict(page='login', login_counter=str(login_counter),
                    came_from=came_from)

    @expose()
    def post_login(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on successful
        authentication or redirect her back to the login page if login failed.

        """
        if not request.identity:
            login_counter = request.environ.get('repoze.who.logins', 0) + 1
            redirect('/login',
                params=dict(came_from=came_from, __logins=login_counter))
        userid = request.identity['repoze.who.userid']
        flash(_('Welcome back, %s!') % userid)
        redirect(came_from)

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('We hope to see you soon!'))
        redirect(came_from)
