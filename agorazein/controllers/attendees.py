# -*- coding: utf-8 -*-

# turbogears imports
from tg import expose, redirect, validate, flash, config, lurl, require, request, abort
from tg.i18n import ugettext as _
from tg import predicates

import tw2.forms as twf
import tw2.core as twc
from tgext.datahelpers.validators import SQLAEntityConverter
from tgext.datahelpers.utils import fail_with

# project specific imports
from agorazein.lib.base import BaseController
from agorazein.model import DBSession, metadata, Talk, User

class AttendeesController(BaseController):
    @expose('agorazein.templates.attendees.index')
    def index(self, **kw):
        speakers = DBSession.query(User).join(Talk).filter(Talk.accepted==True).all()
        return dict(speakers=speakers)