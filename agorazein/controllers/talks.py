# -*- coding: utf-8 -*-

# turbogears imports
from tg import expose, redirect, validate, flash, config, lurl, require, request, abort
from tg.i18n import ugettext as _
from tg import predicates

import tw2.forms as twf
import tw2.core as twc
from tgext.datahelpers.validators import SQLAEntityConverter
from tgext.datahelpers.utils import fail_with

# project specific imports
from agorazein.lib.base import BaseController
from agorazein.model import DBSession, metadata, Talk
from datetime import datetime

TALK_SUBMISSION_DEADLINE = datetime.strptime(config.conference.call4papers_deadline, '%Y-%m-%d')
TALK_TYPES = config.conference.talk_options['types']

class TalkSubmissionForm(twf.TableForm):
    title = twf.TextField(validator=twc.Validator(required=True), css_class='input-xxlarge')
    duration = twf.SingleSelectField(options=TALK_TYPES.items(), prompt_text=None, css_class='input-xxlarge',
                                     validator=twc.OneOfValidator(values=TALK_TYPES.keys()))
    description = twf.TextArea(validator=twc.StringLengthValidator(min=256, max=16384),
                               css_class='input-xxlarge', rows=10)

    submit = twf.SubmitButton(value='Save', css_class='btn btn-primary')
    action = lurl('/talks/create')

class TalkEditForm(TalkSubmissionForm):
    talk = twf.HiddenField(validator=SQLAEntityConverter(Talk))

    action = lurl('/talks/save')

class TalksController(BaseController):
    @expose('agorazein.templates.talks.index')
    def index(self, **kw):
        talks = Talk.get_public_talks()
        return dict(talks=talks)

    @require(predicates.not_anonymous())
    @expose('agorazein.templates.talks.new')
    def new(self, **kw):
        if datetime.utcnow() >= TALK_SUBMISSION_DEADLINE:
            flash('Cannot modify already accepted talk', 'error')
            abort(403)

        return dict(form=TalkSubmissionForm, author=request.identity['user'])

    @require(predicates.not_anonymous())
    @validate(TalkSubmissionForm, error_handler=new)
    @expose()
    def create(self, title, duration, description, **kw):
        if datetime.utcnow() >= TALK_SUBMISSION_DEADLINE:
            flash('Cannot modify already accepted talk', 'error')
            abort(403)

        talk = Talk(title=title, duration=duration, description=description,
                    user=request.identity['user'])
        DBSession.add(talk)
        DBSession.flush()
        flash(_('Your talk has been submitted'))
        return redirect('/talks/view/%s' % talk.slug)

    @require(predicates.not_anonymous())
    @validate(dict(talk=SQLAEntityConverter(Talk)),
              error_handler=fail_with(403))
    @expose('agorazein.templates.talks.edit')
    def edit(self, talk, **kw):
        value = {'talk': talk,
                 'title': talk.title,
                 'description': talk.description,
                 'duration': talk.duration}
        return dict(form=TalkEditForm, value=value)

    @require(predicates.not_anonymous())
    @validate(TalkEditForm, error_handler=edit)
    @expose()
    def save(self, talk, title, duration, description, **kw):
        if not talk.is_owned_by_current_user():
            abort(403)

        if talk.accepted:
            flash('Cannot modify already accepted talk', 'error')
            abort(403)

        talk.title = title
        talk.duration = duration
        talk.description = description

        flash(_('Your talk has been updated'))
        return redirect('/talks/view/%s' % talk.slug)

    @validate(dict(talk=SQLAEntityConverter(Talk, slugified=True)),
              error_handler=fail_with(404))
    @expose('agorazein.templates.talks.view')
    def view(self, talk):
        if not talk.accepted and not talk.is_owned_by_current_user():
            abort(404)

        return dict(talk=talk)

    @require(predicates.in_group('managers'))
    @validate(dict(talk=SQLAEntityConverter(Talk)),
              error_handler=fail_with(404))
    @expose()
    def accept(self, talk):
        talk.accepted = True
        flash(_('Talk marked as accepted'))
        return redirect('/talks')
