# -*- coding: utf-8 -*-

import tg
from markupsafe import Markup
from datetime import datetime
from tgext.pluggable import plug_url

from agorazein.model.calendar import get_talks_calendar
from agorazein.model.posts import get_static_pages

def current_year():
  now = datetime.now()
  return now.strftime('%Y')

def icon(icon_name, white=False):
    if (white):
        return Markup('<i class="icon-%s icon-white"></i>' % icon_name)
    else:
        return Markup('<i class="icon-%s"></i>' % icon_name)

def format_date(date):
    return datetime.strftime(date, '%d %b')

def call4paper_deadline():
    return datetime.strptime(tg.config.conference.call4papers_deadline, '%Y-%m-%d')

def is_call4paper_closed():
    now = datetime.utcnow()
    deadline = call4paper_deadline()
    return deadline <= now

def talks_schedule_url():
    start_day = tg.config.conference.start_day
    calendar = get_talks_calendar()
    return plug_url('calendarevents', '/calendar/%s/agendaWeek/%s' % (calendar.uid, start_day))

def conference_title():
    return tg.config.conference.title

def conference_city():
    return tg.config.conference.city

def conference_payoff():
    return tg.config.conference.payoff

def conference_summary():
    return tg.config.conference.summary

def static_pages():
    return get_static_pages()

def post_image_url(post):
    if not post.attachments:
        return "/images/default_post_icon.png"
    else:
        return post.attachments[0].url