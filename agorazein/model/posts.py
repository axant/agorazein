from tgext.pluggable import app_model

def get_recent_news():
    return app_model.Article.get_published('news').limit(10)

def get_static_pages():
    return app_model.Article.get_published('statics')