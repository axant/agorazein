from agorazein import model
from calendarevents import EventType
from calendarevents.model import Calendar, CalendarEvent
from datetime import datetime

class DeadlineEventType(EventType):
    name = 'deadline'

    def get_linkable_entities(self, calendar):
        return []

    def get_linked_entity_info(self, event):
        return None

    def get_linked_entity_url(self, event):
        return False