from agorazein import model
from calendarevents import EventType
from calendarevents.model import Calendar, CalendarEvent
from datetime import datetime

class TalkEventType(EventType):
    name = 'talk'

    def get_linkable_entities(self, calendar):
        talks = model.DBSession.query(model.Talk)
        return [(t.uid, t.title) for t in talks]

    def get_linked_entity_info(self, event):
        sm = model.DBSession.query(model.Talk).get(event.linked_entity_id)
        if not sm:
            return ''

        return 'INFO EVENTO'

    def get_linked_entity_url(self, event):
        return False