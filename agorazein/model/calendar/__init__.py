from tgext.pluggable import app_model
from .talk import TalkEventType
from .deadline import DeadlineEventType

event_types = [TalkEventType(), DeadlineEventType()]

def get_deadlines_calendar():
    return app_model.DBSession.query(app_model.Calendar).filter_by(events_type=DeadlineEventType.name).first()

def get_talks_calendar():
    return app_model.DBSession.query(app_model.Calendar).filter_by(events_type=TalkEventType.name).first()

def get_upcoming_events(calendar):
    return app_model.DBSession.query(app_model.CalendarEvent)\
        .filter_by(calendar_id=calendar.uid)\
        .order_by(app_model.CalendarEvent.datetime.asc()).limit(5)
