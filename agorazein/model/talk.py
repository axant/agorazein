# -*- coding: utf-8 -*-
from tg import request
from tg.caching import cached_property
from datetime import datetime

from sqlalchemy import *
from sqlalchemy.orm import mapper, relation
from sqlalchemy import Table, ForeignKey, Column
from sqlalchemy.types import Integer, Unicode, DateTime
from sqlalchemy.orm import relation, backref

from agorazein.model import DeclarativeBase, metadata, DBSession
from tgext.datahelpers.utils import slugify

class Talk(DeclarativeBase):
    __tablename__ = 'talk'
    
    uid = Column(Integer, primary_key=True)
    
    created_at = Column(DateTime, nullable=False, default=datetime.utcnow)
    updated_at = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)

    title = Column(Unicode(1024), nullable=False)
    description = Column(Unicode(32768), nullable=False)
    duration = Column(Unicode(64), nullable=False)

    user_id = Column(Integer, ForeignKey('tg_user.user_id'))
    user = relation('User', backref=backref('talks', lazy='dynamic', cascade='all, delete-orphan'))
    accepted = Column(Boolean, default=False)

    @classmethod
    def get_public_talks(cls):
        if request.identity and 'managers' in request.identity['groups']:
            return DBSession.query(Talk)

        return DBSession.query(Talk).filter_by(accepted=True)


    @cached_property
    def slug(self):
        return slugify(self, self.title)

    def is_owned_by_current_user(self):
        if not request.identity or not request.identity.get('user'):
            return False

        if 'managers' in request.identity['groups']:
            return True

        return request.identity['user'].user_id == self.user_id